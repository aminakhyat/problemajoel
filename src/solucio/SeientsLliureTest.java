package solucio;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SeientsLliureTest {

	private int[][] aula;
	private int resultatEsperat;

	public SeientsLliureTest(int[][] aula, int resultat) {
		this.aula = aula;
		this.resultatEsperat = resultat;
	}

	@Parameters
	public static Collection<Object[]> aula() {
		return Arrays.asList(new Object[][] { 
			{ new int[][] { 
				{ 0, 0, 0 }, 
				{ 0, 0, 0 }, 
				{ 0, 0, 0 } }, 6 
			},
			{ new int[][] { 
				{1, 0, 1, 0, 1},
				{0, 0, 0, 1, 0},
				{1, 0, 1, 0, 0} }, 2 
			},
			{ new int[][] { 
				{1, 0, 1, 0, 1},
				{0, 0, 0, 1, 0},
				{1, 0, 1, 0, 1},
				{1, 0, 1, 0, 1}}, 1 
			},
			{ new int[][] { 
				{0, 1, 0, 1, 0},
				{1, 0, 1, 0, 1},
				{0, 0, 1, 0, 0},
				{1, 0, 0, 1, 0}}, 2 
			},
			{ new int[][] { 
				{1, 0, 1},
				{1, 0, 1},
				{1, 0, 1}}, 0 
			},
			{ new int[][] { 
				{0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{1, 0, 1, 0, 1, 0, 1, 0, 1, 0},
				{0, 0, 1, 0, 0, 1, 0, 0, 1, 0},
				{1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
				{0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{1, 0, 1, 0, 1, 0, 1, 0, 1, 0},
				{0, 0, 1, 0, 0, 1, 0, 0, 1, 0},
				{1, 0, 1, 0, 1, 0, 0, 0, 0, 1}}, 16
			}
		});
	}

	@Test
	public void llegirNumeroSeientsLliures() {
		int resultat = SeientsLliure.calculaNumeroSeientsLliures(aula);
		assertEquals(resultatEsperat, resultat);
	}

}
