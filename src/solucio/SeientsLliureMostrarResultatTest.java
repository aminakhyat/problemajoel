package solucio;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SeientsLliureMostrarResultatTest {

	private int seientsABuscar;
	private int seientsLliures;
	private String missatgeEsperat;

	public SeientsLliureMostrarResultatTest(int seientsABuscar, int seientsLliures, String missatgeEsperat) {
		this.seientsABuscar = seientsABuscar;
		this.seientsLliures = seientsLliures;
		this.missatgeEsperat = missatgeEsperat;
	}
	
	@Parameters
	public static Collection<Object[]> seients() {
		return Arrays.asList(new Object[][] {
			{3, 4, "Suficients (4 seients lliures)"},
			{1, 0, "Insuficients (0 seients lliures)"},
			{2, 2, "Suficients (2 seients lliures)"}
		});	
	}


	@Test
	public void testAmbQuantitatSeients() {
		String missatge = SeientsLliure.obtenirResultat(seientsABuscar, seientsLliures);
		assertEquals(missatgeEsperat, missatge);
	}

}