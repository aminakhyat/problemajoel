package solucio;

import java.util.Scanner;

/**
 * 
 * @author Amina Khyat El Achlam
 * @version Mar/2021
 *
 */
public class SeientsLliure {
	static Scanner reader = new Scanner(System.in);

	/**
	 * Programa principal
	 * 
	 * @param args arguments del projecte principal
	 */
	public static void main(String[] args) {

		int[][] aula;
		int casos = reader.nextInt();
		int seientsABuscar;
		while (casos > 0) {
			seientsABuscar = reader.nextInt();
			aula = llegirMatriu();

			int seientsLliures = calculaNumeroSeientsLliures(aula);
			String resultat = obtenirResultat(seientsABuscar, seientsLliures);
			System.out.println(resultat);
			casos--;
		}
		reader.close();
	}

	/**
	 * Mètode que obtè i mostra el resultat segons si hi ha suficients seients
	 * lliures o no.
	 * 
	 * @param seientsABuscar  el número de seients lliures que busca l'usuari
	 * @param contadorLliures el número de seients lliures que hi ha a l'aula
	 * @return string amb un missatge segons si ha seients lliures o no.
	 *         <ul>
	 *         <li>Si hi ha suficients seients lliures: "Suficients(<em>N</em>
	 *         seients lliures)"</li>
	 *         <li>Si no hi ha suficients seients lliures: "Suficients(<em>N</em>
	 *         seients lliures)"</li>
	 *         </ul>
	 * 
	 */
	public static String obtenirResultat(int seientsABuscar, int contadorLliures) {
		if (contadorLliures >= seientsABuscar) {
			return "Suficients (" + contadorLliures + " seients lliures)";
		} else {
			return "Insuficients (" + contadorLliures + " seients lliures)";
		}
	}

	/**
	 * Mètode que llegeix la matriu que represente els seients lliures i ocupats de
	 * l'aula
	 * 
	 * @return matriu amb la distribució dels seients de l'aula segons si estan
	 *         lliures o no
	 */
	public static int[][] llegirMatriu() {
		int[][] aula;
		int columnes;
		int files;
		files = reader.nextInt();
		columnes = reader.nextInt();
		aula = new int[files][columnes];
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				aula[i][j] = reader.nextInt();
			}
		}
		return aula;
	}

	/**
	 * Mètode que calcula el número de seients lliures que poden ser ocupats a
	 * l'aula.
	 * 
	 * @param aula matriu que representa l'aula amb els seients lliures i ocupats
	 * @return int un número que és la quantitat de seients lliures que poden ser
	 *         ocupats que hi ha a l'aula
	 */
	public static int calculaNumeroSeientsLliures(int[][] aula) {
		int seientsLliures = 0;
		for (int i = 0; i < aula.length; i++) {
			for (int j = 0; j < aula[i].length; j++) {
				if (j == 0) {
					if (aula[i][1] == 0 && aula[i][0] == 0) {
						aula[i][j] = 1;
						seientsLliures++;
					}
				} else if (j == aula[i].length - 1) {
					if (aula[i][j - 1] == 0 && aula[i][j] == 0) {
						aula[i][j] = 1;
						seientsLliures++;
					}
				} else if (aula[i][j - 1] == 0 && aula[i][j] == 0 && aula[i][j + 1] == 0) {
					aula[i][j] = 1;
					seientsLliures++;
				}
			}
		}
		return seientsLliures;
	}

}
